# Dark Souls Counter

This command line tool reads the following information for each character in a Dark Souls save
file:

* total number of deaths
* current amount of souls
* current amount of humanity
* the amount of souls in the soul cache (green orb left on the ground after dying) if existent
* the amount of humanity in the soul cache (green orb left on the ground after dying) if existent

In addition, some information cannot directly be found in the save file but is calculated when
a character is played while this program is running:

* the total number of souls the character lost
* the total amount of humanity the character lost

The afore-mentioned information is saved to a JSON file, which is used for initializing it for
each character when starting this tool, i. e. as long as the JSON file is not deleted, the
information is persistent.

The output is both written to console as well as to different files, see [Usage](#usage) for more
information.

## Table of Contents

* [About the Project](#about-the-project)
* [Getting Started](#getting-started)
  * [Prerequisites](#prerequisites)
  * [Installation](#installation)
* [Usage](#usage)
* [License](#license)

## About the project

There are several existing projects and tools that provide the service of extracting the death
counter of a Dark Souls character. Most of them are, however, either Windows specific and don't
work on Linux or websites where you have to upload your save file, which makes that process very
static, i. e. you cannot get a live counter while running the game.

This tool supports Linux (with Steam/Valve's Proton) as well as Windows and is able to get a live
counter for a running game (due to the fact that Dark Souls regularly saves the game).

Furthermore, this tool extracts additional useful information apart from the total number of
deaths.

## Getting Started

You can either download a pre-compiled binary or compile the code yourself. For the pre-compiled
binaries, just visit the [release page](https://gitlab.com/Flobaer/dark_souls_counter/-/releases)
and download the most recent binary for your operating system (Windows or Linux). Jump to
[Usage](#usage) for an overview how to run and use this tool.

If you want to compile the code yourself, read the following sections. This is a standard
[Rust](https://www.rust-lang.org/) project, so if you already know how to compile and run a Rust
project, you can skip the next part and jump straight to [Usage](#usage)

### Prerequisites

You need a Rust compiler toolchain, which consists of the following tools:

* `rustup`: The Rust toolchain installer
* `cargo`: The Rust package manager (installed via `rustup`)
* `rustc`: The Rust compiler (usually invoked via `cargo`)

So you'll need to get `rustup`, with which you can then install the other two tools. If you are on
Linux, the preferred way to get it is the system's package repositories, if possible. Otherwise,
just follow the instructions on the official Rust
[installation guide](https://www.rust-lang.org/learn/get-started).

### Installation

1. Get the source code
  * with git: `git clone https://gitlab.com/Flobaer/dark_souls_counter.git`
  * by downloading it as an [archive](https://gitlab.com/Flobaer/dark_souls_counter/-/archive/master/dark_souls_counter-master.zip) and extracting that
2. Compile the code: open a terminal, navigate to the projects root folder and enter the command
   `cargo build --release`
3. You should find the executable file `dark_souls_counter` or `dark_souls_counter.exe` in `target/release/`

## Usage

With a default Dark Souls installation, you can just launch the executable file. It will
automatically find the save file in use by Dark Souls and regularly extract the death counters and
additional information and print the findings to several files in the folder where the executable
is located until you stop it. The following files are created:

* `DSC_all_characters.txt`: Contains the following information for each character: the name, total
  deaths, current souls, current humanity, total lost souls, total lost humanity, and souls and
  humanity in the soul cache for each character. The content of this file is also printed to the
  console.
* `DSC_active_char_deaths.txt`: Contains the death counter for the character that's currently
  played. This file is useful for e. g. streamers who simply want to include a death counter for
  the current character in their overlay. A value of `-` means that the currently played character
  could not yet be determined.
* `DSC_active_char_lost_souls.txt`: Contains the number of lost souls for the character that's
  currently played. This file is useful for e. g. streamers who simply want to include a counter in
  their overlay for the souls they've lost for that character during the stream. A value of `-`
  means that the currently played character could not yet be determined.
* `DSC_storage.json`: Contains the lost souls and humanity for each character (identified by name
  and save slot) for persistent storage, i. e. for initializing the lost souls and humanity
  counters when starting the program.

You can, however, give a different DS save file, output file or steam library path via command
line. A complete explanation of command line arguments is available via `dark_souls_counter -h`:

```
Dark Souls Death Reader

USAGE:
    dark_souls_counter [OPTIONS]

FLAGS:
    -h, --help       Prints help information
    -V, --version    Prints version information

OPTIONS:
    -i, --input <FILE>           Use this Dark Souls save file instead of the default one. Overrides '-s'.
    -o, --output <FILE>          Use this file to save the death counts read from the Dark Souls save file instead of
                                 the default one (`DS_Deaths_Souls.txt`).
    -s, --steamlibrary <PATH>    Only applicable to Linux. Use this Steam library folder (usually named 'SteamLibrary')
                                 instead of the default one. This is overriden by '-i'.
```

## License

Distributed under the GNU GPLv3 License. See `LICENSE` for more information.
