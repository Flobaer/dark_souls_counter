//! # Dark Souls Counter
//!
//! This command line tool reads the following information for each character in a Dark Souls save
//! file:
//!
//! * total number of deaths
//! * current amount of souls
//! * current amount of humanity
//! * the amount of souls in the soul cache (green orb left on the ground after dying) if existent
//! * the amount of humanity in the soul cache (green orb left on the ground after dying) if existent
//!
//! In addition, some information cannot directly be found in the save file but is calculated when
//! a character is played while this program is running:
//!
//! * the total number of souls the character lost
//! * the total amount of humanity the character lost
//!
//! The afore-mentioned information is saved to a JSON file, which is used for initializing it for
//! each character when starting this tool, i. e. as long as the JSON file is not deleted, the
//! information is persistent.
//!
//! All of the found and calculated information is both written to console as well as to different
//! files, visit the [repository](https://gitlab.com/Flobaer/dark_souls_counter) for more
//! information.

use std::collections::BTreeMap;
use std::convert::TryInto;
use std::fmt;
use std::fs::File;
use std::io::prelude::*;
use std::io::{self, Read, SeekFrom, Write};
use std::path::PathBuf;
use std::process::exit;
use std::thread;
use std::time;

use chrono::Local;
use clap::{value_t, App, Arg, ArgMatches};
use crossterm::{execute, terminal};
use encoding::all::ISO_8859_1;
use encoding::{DecoderTrap, Encoding};
use serde::{Deserialize, Serialize};

/// Stores configuration regarding file paths.
struct Config {
    /// The path to the Dark Souls save file.
    dark_souls_save_file: PathBuf,
    /// The path to the file where information about lost souls is permanently stored.
    persistent_storage_file: PathBuf,
    /// The path to the file where information about all charactes is to be saved.
    all_characters_output_file: PathBuf,
    /// The path to the file where the death counter for the currently played character is to be
    /// saved.
    active_character_deaths_output_file: PathBuf,
    /// The path to the file where the total amount of lost souls for the currently played
    /// character is to be saved.
    active_character_lost_souls_output_file: PathBuf,
}

impl fmt::Display for Config {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        #[cfg(target_os = "linux")]
        let endline = "\n";
        #[cfg(target_os = "windows")]
        let endline = "\r\n";

        let mut string = String::new();

        string.push_str(&format!(
            "Dark Souls save file: {}{}",
            self.dark_souls_save_file.display(),
            endline
        ));

        string.push_str(&format!(
            "Storage file for persistent info: {}{}",
            self.persistent_storage_file.display(),
            endline
        ));
        string.push_str(&format!(
            "Output file for all characters: {}{}",
            self.all_characters_output_file.display(),
            endline
        ));
        string.push_str(&format!(
            "Output file for currently played character's deaths: {}{}",
            self.active_character_deaths_output_file.display(),
            endline
        ));
        string.push_str(&format!(
            "Output file for currently played character's lost souls: {}{}",
            self.active_character_lost_souls_output_file.display(),
            endline
        ));

        write!(f, "{}", string)
    }
}

/// Stores a Dark Souls character's information.
///
/// Apart from `lost_souls` all the information can be found directly in the save file.
#[derive(Clone)]
struct Character {
    /// The name of the character
    name: String,
    /// The total number of deaths of that character.
    deaths: u32,
    /// The amount of souls the character currently has.
    current_souls: u32,
    /// The amount of humanity the character currently has.
    current_humanity: u32,
    /// The total amount of souls the character has lost.
    lost_souls: u32,
    /// The total amount of humanity the character has lost.
    lost_humanity: u32,
    /// The amount of souls contained in the current green orb upon death if existing.
    ///
    /// A value of u32::MAX means that the green orb does not exist.
    soul_cache_souls: u32,
    /// The amount of humanity contained in the current green orb upon death if existing.
    ///
    /// A value of u32::MAX means that the green orb does not exist.
    soul_cache_humanity: u32,
}

impl fmt::Display for Character {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        #[cfg(target_os = "linux")]
        let endline = "\n";
        #[cfg(target_os = "windows")]
        let endline = "\r\n";

        let mut string = format!("{}:", self.name);
        string.push_str(endline);

        string.push_str(&format!("  {} total death", self.deaths));
        if self.deaths != 1 {
            string.push_str("s");
        }
        string.push_str(endline);

        string.push_str(&format!(
            "  {}/{} current soul",
            self.current_souls, self.current_humanity
        ));
        if self.current_souls != 1 {
            string.push_str("s");
        }
        string.push_str("/humanity");
        string.push_str(endline);

        string.push_str(&format!(
            "  {}/{} lost soul",
            self.lost_souls, self.lost_humanity
        ));
        if self.current_souls != 1 {
            string.push_str("s");
        }
        string.push_str("/humanity");
        string.push_str(endline);

        match self.soul_cache_souls {
            u32::MAX => string.push_str("  no soul cache"),
            _ => {
                string.push_str(&format!(
                    "  {}/{} soul",
                    self.soul_cache_souls, self.soul_cache_humanity
                ));
                if self.soul_cache_souls != 1 {
                    string.push_str("s");
                }
                string.push_str("/humanity in soul cache");
            }
        };
        string.push_str(endline);

        write!(f, "{}", string)
    }
}

/// Struct for handling the currently known information about all characters within the save file.
///
/// The information of the last read of the save file is stored in addition to the current one in
/// order to determine the currently played character and keep track of lost souls.
struct State {
    /// The characters' information from the current (or most recent) read of the save file.
    ///
    /// A value of `None` for a character/element means that the respective save slot (identified by
    /// the vector's index) contains no character.
    current_characters: Vec<Option<Character>>,
    /// The characters' information from the previous (or last) read of the save file.
    ///
    /// A value of `None` for a character/element means that the respective save slot (identified by
    /// the vector's index) contains no character.
    previous_characters: Vec<Option<Character>>,
    /// The index of the character that is currently being played.
    ///
    /// A value of `None` means that the currently played character could not be determined (yet).
    active_character: Option<usize>,
}

impl State {
    /// Initializes an empty State struct (filled with `None` equivalent values).
    fn new() -> Self {
        State {
            current_characters: vec![None; 10],
            previous_characters: vec![None; 10],
            active_character: None,
        }
    }
}

impl Default for State {
    fn default() -> Self {
        State::new()
    }
}

/// Struct for handling the characters' information that cannot be read directly from the save file
/// and needs to be stored and updated manually.
#[derive(Deserialize, Serialize)]
#[serde(rename_all = "camelCase")]
struct PersistentInfo {
    /// The version of the characters' object structure. Used to check compatibility with an
    /// existing storage file.
    version: String,
    /// The characters' information that needs to be stored and updated separately.
    ///
    /// The mappings have the following meaning, from outermost to innermost HashMap:
    /// 1. Mapping of save slot index to characters.
    /// 2. Mapping of character name to occurrences of losing souls.
    /// 3. Mapping of date of occurrence to amount of souls and humanity lost.
    characters: BTreeMap<u32, BTreeMap<String, BTreeMap<String, (u32, u32)>>>,
}

/// Returns the default file path where the Dark Souls save file is located under Linux.
///
/// # Arguments
///
/// * `matches`: The parsed command-line arguments. Used for specifying a non-default Steam library
///   location
///
/// # Errors
///
/// Returns an error if the home directory of the user could not be found.
#[cfg(target_os = "linux")]
fn get_save_file_location(matches: ArgMatches) -> Result<PathBuf, String> {
    let default_steam_library = match dirs::home_dir() {
        Some(path) => path.join(PathBuf::from(".local/share/Steam")),
        None => return Err(String::from("home directory not found")),
    };

    let default_ds_save_file = PathBuf::from(
        "steamapps/compatdata/211420/pfx/drive_c/users/steamuser/My Documents/NBGI/DarkSouls/DRAKS0005.sl2",
    );

    let input_file = match matches.value_of("steamlibrary") {
        Some(path) => PathBuf::from(path).join(default_ds_save_file),
        None => default_steam_library.join(default_ds_save_file),
    };

    Ok(input_file)
}

/// Returns the default file path where the Dark Souls save file is located under Windows.
///
/// # Arguments
///
/// * `_matches`: The parsed command-line arguments. Currently unused.
///
/// # Errors
///
/// Returns an error if the home directory of the user could not be found.
#[cfg(target_os = "windows")]
fn get_save_file_location(_matches: ArgMatches) -> Result<PathBuf, String> {
    let input_file = match dirs::home_dir() {
        Some(path) => path.join(PathBuf::from(r"Documents\NBGI\DarkSouls\DRAKS0005.sl2")),
        None => return Err(String::from("home directory not found")),
    };

    Ok(input_file)
}

/// Initializes configuration with command-line arguments.
fn setup_config() -> Config {
    let default_persistent_storage_file = PathBuf::from("DSC_storage.json");
    let default_output_file = PathBuf::from("DSC_all_characters.txt");
    let default_active_character_deaths_output_file = PathBuf::from("DSC_active_char_deaths.txt");
    let default_active_character_lost_souls_output_file =
        PathBuf::from("DSC_active_char_lost_souls.txt");

    let matches = App::new("Dark Souls Counter")
        .version("1.7.0")
        .author("Florens Werner <gitlab@fwerner.eu>")
        .about("Displays character info from Dark Souls save file")
        .arg(
            Arg::with_name("steamlibrary")
                .help(
                    "Only applicable to Linux. Use this Steam library folder (usually named \
                    'SteamLibrary') instead of the default one. This is overriden by '-i'.",
                )
                .short("s")
                .long("steamlibrary")
                .takes_value(true)
                .value_name("PATH"),
        )
        .arg(
            Arg::with_name("input")
                .help("Use this Dark Souls save file instead of the default one. Overrides '-s'.")
                .short("i")
                .long("input")
                .takes_value(true)
                .value_name("FILE"),
        )
        .arg(
            Arg::with_name("output")
                .help(&format!(
                    "Use this file to save the death counts read from the Dark Souls save file \
                    instead of the default one ({}).",
                    default_output_file.to_str().unwrap_or("")
                ))
                .short("o")
                .long("output")
                .takes_value(true)
                .value_name("FILE"),
        )
        .get_matches();

    let output_file = clap::value_t!(matches, "output", PathBuf).unwrap_or(default_output_file);
    let input_file = match matches.value_of("input") {
        Some(path) => PathBuf::from(path),
        None => match get_save_file_location(matches) {
            Ok(save_file) => save_file,
            Err(error) => {
                println!("Error: {}", error);
                exit(1);
            }
        },
    };

    Config {
        dark_souls_save_file: input_file,
        persistent_storage_file: default_persistent_storage_file,
        all_characters_output_file: output_file,
        active_character_deaths_output_file: default_active_character_deaths_output_file,
        active_character_lost_souls_output_file: default_active_character_lost_souls_output_file,
    }
}

/// Returns the names of the characters found in the Dark Souls save file.
///
/// A value of `None` for a character's name means that the respective save slot (identified by the
/// vector's index) contains no character. The return vector always contains 10 elements (for the
/// 10 save slots).
///
/// # Arguments
///
/// * `file`: The path where the Dark Souls save file is located.
///
/// # Aborts
///
/// The program terminates if the save file does not exist, a byte index cannot be sought, the file
/// cannot be read or a character's name cannot be decoded from bytes to a string.
///
/// # Dark Souls save file format
///
/// The first byte index of the first save slot's character's name is `0x3c0`. Each character is
/// spaced `0x60020` bytes from the previous one. A character's name is 26 bytes long and encoded
/// in ISO-8859-1 (latin1), which uses 2 bytes per character (i. e. the name is 13 characters
/// long), a shorter name is indicated by ending with two consecutive null bytes (`0x0000`). The
/// save file always contains 10 save slots; if a save slot is unused, the name contains only null
/// bytes. The entire file is little-endian.
fn get_character_names(file: &PathBuf) -> Vec<Option<String>> {
    let first_character_field = 0x3c0;
    let character_name_offset = 0x60020;

    let mut character_names = vec![None; 10];
    let mut character_field = vec![0u8; 26];
    let mut file = match File::open(file) {
        Err(reason) => {
            println!("Error: input file does not exist: {}", reason);
            exit(1);
        }
        Ok(file) => file,
    };

    for character_counter in 0..10 {
        let seek_index = first_character_field + character_counter * character_name_offset;

        if let Err(reason) = file.seek(SeekFrom::Start(seek_index)) {
            println!(
                "Error: could not seek first name byte ({}) of save slot {}: {}",
                seek_index, character_counter, reason
            );
            exit(1);
        };

        if let Err(reason) = file.read_exact(&mut character_field) {
            println!(
                "Error: could not read name bytes of character {}: {}",
                character_counter, reason
            );
            exit(1);
        };

        let mut character_bytes: Vec<u8> = Vec::with_capacity(26);

        // normalize character name by stripping null bytes
        let mut zero_byte_counter = 0;
        for byte in character_field.iter() {
            // process only bytes that are not null as they are not needed for decoding to string
            // (latin1 only uses 1 byte effectively)
            if *byte == 0u8 {
                zero_byte_counter += 1;
                // two consecutive null bytes indicate end of the name => discard remaining bytes
                if zero_byte_counter == 2 {
                    break;
                }
            } else {
                character_bytes.push(*byte);
                zero_byte_counter = 0;
            }
        }

        if !character_bytes.is_empty() {
            let character_name = match ISO_8859_1.decode(&character_bytes, DecoderTrap::Strict) {
                Err(reason) => {
                    println!(
                        "Error: character name could not be decoded from bytes to string (Latin 1 encoding): {}",
                        reason
                    );
                    exit(1);
                }
                Ok(name) => name,
            };
            character_names[character_counter as usize] = Some(character_name);
        }
    }

    character_names
}

/// Returns the save slot index for the currently played character.
///
/// Returns the previous index for the active character if the currently played character could not
/// be determined.
///
/// # Arguments
///
/// * `state`: The currently known information drawn from Dark Souls save file reads.
fn get_active_character_index(state: &State) -> Option<usize> {
    let mut active_character_index = None;
    let mut previous_characters_empty = true;
    let character_iterator = state
        .current_characters
        .iter()
        .zip(state.previous_characters.iter())
        .enumerate();
    for (character_index, characters) in character_iterator {
        match characters {
            // character exists now and existed previously => check if values differ
            (Some(current_character), Some(previous_character)) => {
                previous_characters_empty = false;
                // the currently played character is determined by finding differing values for
                // number of deaths, amount of current souls or amount of souls in the green orb
                // between two save file reads
                if current_character.deaths != previous_character.deaths
                    || current_character.current_souls != previous_character.current_souls
                    || current_character.current_humanity != previous_character.current_humanity
                    || current_character.soul_cache_souls != previous_character.soul_cache_souls
                    || current_character.soul_cache_humanity
                        != previous_character.soul_cache_humanity
                {
                    active_character_index = Some(character_index);
                }
            }
            // character exists now and didn't exist previously => return existing
            (Some(_), None) => active_character_index = Some(character_index),
            // character was deleted => no character is active
            // TODO: check correctness (=> None?)
            (None, Some(_)) => active_character_index = Some(character_index),
            _ => (),
        };
    }

    // catch bootstrapping case, i. e. first read of the file (which means
    // previous_characters is a vector containing only None values
    if previous_characters_empty {
        active_character_index = None;
    }

    // return previous value for the active character if the current one could not be determined,
    // i. e. if no value differs for any character between to save file reads
    match active_character_index {
        Some(_) => active_character_index,
        None => state.active_character,
    }
}

/// Initializes each character's total amount of lost souls and humanity with previously collected information.
///
/// # Arguments
///
/// * `state`: The struct containing the characters to be initialized with the previously collected
///   information regarding lost souls and humanity. This struct is updated in-place.
/// * `persistent_info`: The previously collected information containing the lost and humanity.
fn initialize_lost_souls_humanity(state: &mut State, persistent_info: &PersistentInfo) {
    for (index, optional_character) in state.previous_characters.iter_mut().enumerate() {
        // only check for previously collected info for save slots that contain a character
        if let Some(session_character) = optional_character {
            // implicitly check if there is a character in the previously collected information at
            // the same save slot as the currently processed character in the last save file read
            if let Some(persistent_characters) = persistent_info.characters.get(&(index as u32)) {
                // implicitly check if there is a character with the same name as the currently
                // processed character in the last save file read, i. e. they are considered to be
                // the same character if their names match and they are in the same save slot
                if let Some(persistent_lost_info) =
                    persistent_characters.get(&session_character.name)
                {
                    // accumulate all the previously lost souls and humanity for the character
                    let mut lost_souls = 0;
                    let mut lost_humanity = 0;
                    for (_date, lost_info) in persistent_lost_info.iter() {
                        lost_souls += lost_info.0;
                        lost_humanity += lost_info.1;
                    }
                    session_character.lost_souls = lost_souls;
                    session_character.lost_humanity = lost_humanity;
                }
            }
        }
    }
}

/// Returns the permanently lost souls and humanity of the active character since the last save file read.
///
/// # Arguments
///
/// * `state`: The currently known information drawn from Dark Souls save file reads.
fn get_lost_souls_humanity(state: &State) -> (u32, u32) {
    let mut lost_souls = 0;
    let mut lost_humanity = 0;

    if let Some(index) = state.active_character {
        if let (Some(character), Some(previous_character)) = (
            &state.current_characters[index],
            &state.previous_characters[index],
        ) {
            // Souls and humanity are lost when the character has died and the soul cache contains
            // a valid value. A value of u32::MAX for the soul cache indicates that no soul cache
            // exists.
            if character.deaths > previous_character.deaths
                && character.soul_cache_souls != u32::MAX
            {
                lost_souls = previous_character.soul_cache_souls;
                lost_humanity = previous_character.soul_cache_humanity;
            }
        }
    }

    (lost_souls, lost_humanity)
}

/// Updates the permanently lost souls and humanity info in the state struct and the struct for persistent info.
///
/// # Arguments
///
/// * `state`: The currently known information about the characters that is to be updated with the
///   new loss of souls and/or humanity.
/// * `persistent_info`: The previously collected information about the characters that is to be
///   updated with the new loss of souls and/or humanity.
/// * `lost_souls`: The amount of souls lost.
/// * `lost_humanity`: The amount of humanity lost.
fn update_lost_souls_humanity(
    state: &mut State,
    persistent_info: &mut PersistentInfo,
    lost_souls: u32,
    lost_humanity: u32,
) {
    if let Some(index) = state.active_character {
        if let Some(session_character) = &mut state.current_characters[index] {
            session_character.lost_souls += lost_souls;
            session_character.lost_humanity += lost_humanity;

            // add active character to persistent info if not already existent
            match persistent_info.characters.get_mut(&(index as u32)) {
                // save slot of the active character contains characters in persistent file
                Some(persistent_characters) => {
                    // save slot contains a character with the name of the active character
                    if persistent_characters
                        .get_mut(&session_character.name)
                        .is_none()
                    {
                        persistent_characters
                            .insert(session_character.name.to_string(), BTreeMap::new());
                    }
                }
                // save slot of the active character does not contain any characters
                None => {
                    let mut persistent_character = BTreeMap::new();
                    persistent_character
                        .insert(session_character.name.to_string(), BTreeMap::new());
                    persistent_info
                        .characters
                        .insert(index as u32, persistent_character);
                }
            };

            // update lost souls for persistent storage
            if let Some(persistent_characters) = persistent_info.characters.get_mut(&(index as u32))
            {
                if let Some(persistent_character) =
                    persistent_characters.get_mut(&session_character.name)
                {
                    let now = Local::now();
                    persistent_character.insert(now.to_rfc3339(), (lost_souls, lost_humanity));
                };
            }
        };
    };
}

/// Returns current information for all characters by reading the save file.
///
/// Information for all the fields in the [Character](struct.Character.html) struct is extracted,
/// except for `lost_souls` and `lost_humanity`, which take either their previous values or 0 if
/// the character did not exist previously.
///
/// # Arguments
///
/// * `file`: The path where the Dark Souls save file is located.
/// * `state`: The previous information about characters for getting their current amount of lost
///   souls and humanity.
///
/// # Aborts
///
/// The program terminates if the save file does not exist, a byte index cannot be sought or the
/// file cannot be read.
///
/// # Dark Souls save file format
///
/// The Dark Souls save file contains relevant information for the first save slot (= first
/// character) at the following byte indexes:
///
/// * `0x1f3e8`: The first byte of the characters' death counter.
/// * `0x3ac`: The first byte of the characters' current souls counter.
/// * `0x39c`: The first byte of the characters' current humanity counter.
/// * `0x1e791`: The first byte of the souls contained in the soul cache (green orb on the ground
///   after death)
/// * `0x1e78d`: The first byte of the humanity contained in the soul cache (green orb on the ground
///   after death)
///
/// Each value is a 32 bit integer in little-endian. Each character('s info) is spaced `0x60020`
/// bytes from the previous one.
fn get_current_characters(file: &PathBuf, state: &State) -> Vec<Option<Character>> {
    let first_char_death = 0x1f3e8;
    let first_char_souls = 0x3ac;
    let first_char_humanity = 0x39c;
    let first_char_soul_cache_souls = 0x1e791;
    let first_char_soul_cache_humanity = 0x1e78d;
    let next_char_offset = 0x60020;

    let character_names = get_character_names(file);

    let mut characters: Vec<Option<Character>> = vec![None; 10];
    let mut death_bytes = [0, 0, 0, 0];
    let mut souls_bytes = [0, 0, 0, 0];
    let mut soul_cache_souls_bytes = [0, 0, 0, 0];
    let mut soul_cache_humanity_bytes = [0, 0, 0, 0];

    let mut file = match File::open(file) {
        Err(reason) => {
            println!("Error: input file does not exist: {}", reason);
            exit(1);
        }
        Ok(file) => file,
    };

    for (character_counter, optional_character_name) in character_names.iter().enumerate() {
        // if a character exists in that save slot
        if let Some(character_name) = optional_character_name {
            let name = character_name.to_string();
            let deaths;
            let current_souls;
            let current_humanity;
            let soul_cache_souls;
            let soul_cache_humanity;

            // get death counter
            let seek_index = (first_char_death + character_counter * next_char_offset)
                .try_into()
                .unwrap();

            if let Err(reason) = file.seek(SeekFrom::Start(seek_index)) {
                println!(
                    "Error: could not seek first death counter byte ({}) of character {}: {}",
                    seek_index, character_counter, reason
                );
                exit(1);
            };

            if let Err(reason) = file.read_exact(&mut death_bytes) {
                println!(
                    "Error: could not read death counter bytes of character {}: {}",
                    character_counter, reason
                );
                exit(1);
            };

            deaths = u32::from_le_bytes(death_bytes);

            // get souls counter
            let seek_index = (first_char_souls + character_counter * next_char_offset)
                .try_into()
                .unwrap();

            if let Err(reason) = file.seek(SeekFrom::Start(seek_index)) {
                println!(
                    "Error: could not seek first souls counter byte ({}) of character {}: {}",
                    seek_index, character_counter, reason
                );
                exit(1);
            };

            if let Err(reason) = file.read_exact(&mut souls_bytes) {
                println!(
                    "Error: could not read souls counter bytes of character {}: {}",
                    character_counter, reason
                );
                exit(1);
            };

            current_souls = u32::from_le_bytes(souls_bytes);

            // get humanity counter
            let seek_index = (first_char_humanity + character_counter * next_char_offset)
                .try_into()
                .unwrap();

            if let Err(reason) = file.seek(SeekFrom::Start(seek_index)) {
                println!(
                    "Error: could not seek first humanity counter byte ({}) of character {}: {}",
                    seek_index, character_counter, reason
                );
                exit(1);
            };

            if let Err(reason) = file.read_exact(&mut souls_bytes) {
                println!(
                    "Error: could not read humanity counter bytes of character {}: {}",
                    character_counter, reason
                );
                exit(1);
            };

            current_humanity = u32::from_le_bytes(souls_bytes);

            // get soul cache souls
            let seek_index = (first_char_soul_cache_souls + character_counter * next_char_offset)
                .try_into()
                .unwrap();

            if let Err(reason) = file.seek(SeekFrom::Start(seek_index)) {
                println!(
                    "Error: could not seek first soul cache souls byte ({}) of character {}: {}",
                    seek_index, character_counter, reason
                );
                exit(1);
            };

            if let Err(reason) = file.read_exact(&mut soul_cache_souls_bytes) {
                println!(
                    "Error: could not read soul cache bytes of character {}: {}",
                    character_counter, reason
                );
                exit(1);
            };

            soul_cache_souls = u32::from_le_bytes(soul_cache_souls_bytes);

            // get soul cache humanity
            let seek_index = (first_char_soul_cache_humanity
                + character_counter * next_char_offset)
                .try_into()
                .unwrap();

            if let Err(reason) = file.seek(SeekFrom::Start(seek_index)) {
                println!(
                    "Error: could not seek first soul cache humanity byte ({}) of character {}: {}",
                    seek_index, character_counter, reason
                );
                exit(1);
            };

            if let Err(reason) = file.read_exact(&mut soul_cache_humanity_bytes) {
                println!(
                    "Error: could not read soul cache humanity bytes of character {}: {}",
                    character_counter, reason
                );
                exit(1);
            };

            soul_cache_humanity = u32::from_le_bytes(soul_cache_humanity_bytes);

            // get lost souls
            let lost_souls = match &state.previous_characters[character_counter] {
                Some(character) => character.lost_souls,
                None => 0,
            };

            // get lost humanity
            let lost_humanity = match &state.previous_characters[character_counter] {
                Some(character) => character.lost_humanity,
                None => 0,
            };

            characters[character_counter] = Some(Character {
                name,
                deaths,
                current_souls,
                current_humanity,
                lost_souls,
                lost_humanity,
                soul_cache_souls,
                soul_cache_humanity,
            });
        };
    }

    characters
}

/// Writes the current information about all characters to file.
///
/// The output file will always be overwritten if there is an existing one with the same name.
///
/// # Arguments
///
/// * `current_characters`: The characters to be written to file.
/// * `output_file`: The path to the file where to write the characters to.
///
/// # Aborts
///
/// The program terminates if the output file could not be created or written to.
fn write_info_to_file(current_characters: &[Option<Character>], output_file: &PathBuf) {
    let display = output_file.display();
    let mut file = match File::create(&output_file) {
        Err(reason) => {
            println!("Error: could not create {}: {}", display, reason);
            exit(1);
        }
        Ok(file) => file,
    };

    let mut output_string = String::new();
    for optional_character in current_characters.iter() {
        if let Some(character) = optional_character {
            output_string.push_str(&character.to_string());
            #[cfg(target_os = "linux")]
            output_string.push_str("\n");
            #[cfg(target_os = "windows")]
            output_string.push_str("\r\n");
        }
    }

    if let Err(reason) = file.write_all(output_string.as_bytes()) {
        println!("Error: could not write to {}: {}", display, reason);
        exit(1);
    };
}

/// Writes the death and lost souls counters of the currently played character to a separate file each.
///
/// # Arguments
///
/// * `state`: The state information that contains the currently played character.
/// * `output_file_deaths`: The path to the file where to write the death counter of the currently
///   played character.
/// * `output_file_lost_souls`: The path to the file where to write the lost souls counter of the
///   currently played character.
///
/// # Aborts
///
/// The program terminates if one of the output files could not be created or written to.
fn write_active_char_to_file(
    state: &State,
    output_file_deaths: &PathBuf,
    output_file_lost_souls: &PathBuf,
) {
    let mut death_counter_string = String::from("-");
    let mut lost_souls_counter_string = String::from("-");

    if let Some(index) = state.active_character {
        if let Some(character) = &state.current_characters[index] {
            death_counter_string = character.deaths.to_string();
            lost_souls_counter_string = character.lost_souls.to_string();
        }
    }

    let mut file = match File::create(&output_file_deaths) {
        Err(reason) => {
            println!(
                "Error: could not create {}: {}",
                output_file_deaths.display(),
                reason
            );
            exit(1);
        }
        Ok(file) => file,
    };

    if let Err(reason) = file.write_all(death_counter_string.as_bytes()) {
        println!(
            "Error: could not write to {}: {}",
            output_file_deaths.display(),
            reason
        );
        exit(1);
    };

    let mut file = match File::create(&output_file_lost_souls) {
        Err(reason) => {
            println!(
                "Error: could not create {}: {}",
                output_file_lost_souls.display(),
                reason
            );
            exit(1);
        }
        Ok(file) => file,
    };

    if let Err(reason) = file.write_all(lost_souls_counter_string.as_bytes()) {
        println!(
            "Error: could not write to {}: {}",
            output_file_lost_souls.display(),
            reason
        );
        exit(1);
    };
}

/// Writes the current information about all characters and file paths to the terminal.
///
/// # Arguments
///
/// * `state`: The current information about the characters to be written to terminal.
/// * `config`: The current configuration (about file paths) to be written to terminal.
///
/// # Aborts
///
/// The program terminates if the terminal could not be cleared.
fn write_info_to_terminal(state: &State, config: &Config) {
    if let Err(reason) = execute!(io::stdout(), terminal::Clear(terminal::ClearType::All)) {
        println!("Error: could not clear terminal: {}", reason);
        exit(1);
    }

    println!("{}", config.to_string());
    for optional_character in state.current_characters.iter() {
        if let Some(character) = optional_character {
            println!("{}", character);
        }
    }
}

/// Reads and extracts permanent information about lost souls and humanity from file.
///
/// The input file is expected to contain valid JSON as described in
/// [write_storage_file](fn.write_storage_file.html).
///
/// # Arguments
///
/// * `file`: The path to the file where the previously collected information about lost souls and
///   humanity is stored.
///
/// # Returns
///
/// A `PersistentInfo` struct containing either:
///
/// * no character information if the storage file could not be read or contains no characters
/// * character information about lost souls and humanity for each character found in the storage
///   file
fn read_storage_file(file: &PathBuf) -> PersistentInfo {
    let current_version = "2";
    let mut persistent_info = PersistentInfo {
        version: current_version.to_string(),
        characters: BTreeMap::new(),
    };

    let mut optional_file = match File::open(file) {
        Err(reason) => {
            println!(
                "Warning: could not open file for previously collected info: {}",
                reason
            );
            None
        }
        Ok(file) => Some(file),
    };

    if let Some(ref mut file) = optional_file {
        let mut file_content = String::new();
        match file.read_to_string(&mut file_content) {
            Err(reason) => println!(
                "Warning: could not read content of file for previously collected info: {}",
                reason
            ),
            Ok(_) => match serde_json::from_str(&file_content) {
                Err(reason) => println!(
                    "Warning: could not parse content of file for previously collected info to json: {}",
                    reason
                ),
                Ok(deserialized) => {
                    let deserialized_info: PersistentInfo = deserialized;
                    if deserialized_info.version != current_version {
                        println!(
                            "Warning: version of data format in file containing previously \
                            collected info doesn't match currently supported version ({} vs. {})",
                            deserialized_info.version, current_version
                        );
                    } else {
                        persistent_info = deserialized_info
                    }
                }
            },
        }
    }

    persistent_info
}

/// Writes the permanent information about characters' lost souls and humanity to file.
///
/// The input file is expected to contain valid JSON of one object with the attributes `version`
/// (in case the storage format changes in the future) and `characters`. The `characters` value is
/// an object mapping from save slot numbers (as strings) to character objects, as each save slot
/// could potentially have permanent information about multiple characters if multiple save files
/// are/were used. A character object maps from its name to an object mapping from date strings
/// (when the loss of souls and/or humanity occurred) to the amount of souls and humanity lost.
///
/// An example for JSON file content:
///
/// ```JSON
/// {
///   "version": "2",
///   "characters":
///   {
///     "0": // the first save slot
///     {
///       "John": // the name of one character in the first save slot
///       {
///         "2020-09-19T03:09:42.034680368+02:00": [80, 1], // John lost 80 souls and 1 humanity on 19 September 2020
///         "2020-09-18T03:28:46.337656602+02:00": [7324, 5]
///       },
///       "Jane": // the name of another character in the first save slot
///       {
///         "2020-09-19T03:09:42.034680368+02:00": [1600, 0]
///       }
///     },
///     "1": // the second save slot
///     {
///       "Agneta": // the name of one character in the second save slot
///       {
///         "2020-09-18T03:36:02.958018223+02:00": [29973, 0],
///         "2020-09-18T03:36:59.986905691+02:00": [4977, 8]
///       }
///     }
///   }
/// }
/// ```
///
/// # Arguments
///
/// * `persistent_info`: The information about all characters that have lost souls and/or humanity.
/// * `file`: The file where to permanently save the information about lost souls and humanity.
fn write_storage_file(persistent_info: &PersistentInfo, file: &PathBuf) {
    let mut optional_file = match File::create(&file) {
        Err(reason) => {
            println!("Error: could not create storage file: {}", reason);
            None
        }
        Ok(file) => Some(file),
    };

    if let Some(ref mut file) = optional_file {
        let info_serialized = match serde_json::to_string_pretty(persistent_info) {
            Ok(json) => json,
            Err(reason) => format!(
                "Error while trying to serialize character info to json: {}",
                reason
            ),
        };
        if let Err(reason) = file.write_all(info_serialized.as_bytes()) {
            println!("Error: could not write to storage file: {}", reason);
        };
    }
}

/// Continuously reads the Dark Souls save file and extracts and shows information about characters.
///
/// The extracted information consists of the name, total number of deaths, current amount of
/// souls, amount of souls in the soul cache (green orb on ground upon death), amount of humanity
/// in the soul cache, total amount of lost souls for each character and total amount of lost
/// humanity and writes that information to file(s) and the terminal.
fn main() {
    let config = setup_config();
    println!("{}", config);

    // the save file is read every second
    let sleep_duration = time::Duration::from_secs(1);

    let mut state = State::new();

    state.previous_characters = get_current_characters(&config.dark_souls_save_file, &state);
    let mut persistent_info = read_storage_file(&config.persistent_storage_file);
    initialize_lost_souls_humanity(&mut state, &persistent_info);

    println!();

    loop {
        state.current_characters = get_current_characters(&config.dark_souls_save_file, &state);
        state.active_character = get_active_character_index(&state);
        let lost_souls_humanity = get_lost_souls_humanity(&state);
        if lost_souls_humanity.0 > 0 || lost_souls_humanity.1 > 0 {
            update_lost_souls_humanity(
                &mut state,
                &mut persistent_info,
                lost_souls_humanity.0,
                lost_souls_humanity.1,
            );
            write_storage_file(&persistent_info, &config.persistent_storage_file);
        }

        write_info_to_file(
            &state.current_characters,
            &config.all_characters_output_file,
        );
        write_active_char_to_file(
            &state,
            &config.active_character_deaths_output_file,
            &config.active_character_lost_souls_output_file,
        );
        write_info_to_terminal(&state, &config);

        state.previous_characters = state.current_characters.clone();
        thread::sleep(sleep_duration);
    }
}
